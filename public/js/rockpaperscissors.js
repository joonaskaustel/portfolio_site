function play(){
    //Declare variables
    var user = document.querySelector('input[name="choice"]:checked').getAttribute('id');
    var options = ['rock', 'paper', 'scissors'];
    var computerChoice = options[ Math.floor( Math.random() * options.length ) ];
    var winner;

    //Game logic
    if ( user === 'rock' ){
        if ( computerChoice === 'paper' ) {
            winner = 'computer';
        } else if (computerChoice === 'scissors'){
            winner = 'user';
        } else {
            winner = 'draw';
        }
    } else if (user === 'paper') {
        if (computerChoice === 'scissors') {
            winner = 'computer';
        } else if (computerChoice === 'rock'){
            winner = 'user';
        } else {
            winner = 'draw';
        }
    } else if (user === 'scissors') {
        if (computerChoice === 'rock') {
            winner = 'computer';
        } else if (computerChoice === 'paper'){
            winner = 'user';
        } else {
            winner = 'draw';
        }
    }

    //Display result
    document.getElementById('computerChoice').textContent = computerChoice;
    document.getElementById('winner').textContent = winner;
}

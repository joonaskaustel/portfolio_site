var bill, tip, tipResult;

bill = document.getElementById("billSum");
tipPercent = document.getElementById("tipPercentage");
tipResult = document.getElementById("tipResult").innerHTML;

tipResult = "ello";

//EVENTS
bill.addEventListener("change", function(){
    calculateTip(bill.value, tipPercent.value);
});

tipPercent.addEventListener("input", function(){
    console.log(tipPercent.value);
    calculateTip(bill.value, tipPercent.value);
});

//FUNCTIONS
function calculateTip(bill, tip){
    bill = parseInt(bill);
    tip = parseInt(tip);

    tipSum = Math.round(bill * (tip/100)) / 1;
    total = bill + tipSum;
    
    document.getElementById("tipPercent").innerHTML = tip;
    document.getElementById("tipResult").innerHTML = tipSum;
    document.getElementById("total").innerHTML = total;
}


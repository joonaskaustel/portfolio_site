//SETUP
var express = require("express");
var app = express();
var request = require('request');

app.use(express.static("public"));
app.set("view engine", "ejs");

//ROUTES
app.get("/", function (req, res) {
	res.render("index");
});

app.get('/portfolio', function (req, res) {
	res.render('portfolio');
});

app.get('/tipcalculator', function (req, res) {
	res.render("tipCalculator");
});

app.get("/rockpaperscissors", function (req, res) {
	res.render("rockpaperscissors");
});

app.get("/budgety", function (req, res) {
	res.render("budgety");
});

app.get("/omdb/results", function (req, res) {
	var apiUrl = 'http://www.omdbapi.com/?i=tt3896198&apikey=5a751e15';
	request('http://www.omdbapi.com/?i=tt3896198&apikey=5a751e15', function (error, response, body) {
		if (!error && response.statusCode == 200) {
			var result = JSON.parse(body);
			res.send(result['Title']);
		}
	});
});

//LISTEN
app.listen(3000, function () {
	console.log("Portfolio started");
});